function defaults(obj,defaultprops){
    if(!obj || !defaultprops){
        return null
    }else{
        for (let key in obj){
            if (obj[key] === undefined ){
                obj[key]=defaultprops[key]
            }else{
                obj[key]=obj[key]
            }
        }return obj
    }
    
    
}

module.exports = defaults;
