function pairs(obj){
    if(!obj){
        return null
    }else{
        let pair = [];
        for(let key in obj){
            pair.push([key,obj[key]])
        }return pair;
    }
}


module.exports = pairs;
